package sk.murin.dictionary_quiz;

import java.util.Arrays;

public class Word {

    public String wordInTitle;
    public String [] option;
    public int correctWordPosition;


    public Word(String wordInTitle, String[] option, int correctWordPosition) {
        this.wordInTitle = wordInTitle;
        this.option = option;
        this.correctWordPosition = correctWordPosition;
    }

    @Override
    public String toString() {
        return wordInTitle +" "+ Arrays.toString(option) +" "+ correctWordPosition;
    }
}
