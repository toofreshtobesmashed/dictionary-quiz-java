package sk.murin.dictionary_quiz;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class DictionaryQuiz implements ActionListener {
    private JLabel labelTitle;
    private boolean canGoToNextQuestion = false;
    private JButton btnSubmit;
    private final Random r = new Random();
    private int position = 0;
    private final Font f = new Font("courier", Font.BOLD, 16);
    private final Color defaultBackground = new JRadioButton().getBackground();
    private final JRadioButton[] jRadioButtons = new JRadioButton[3];
    private ButtonGroup buttonGroup;
    private final Word[] words = {                // words[position].option[words[position].correctWord]
            new Word("Car", new String[]{"auto", "motorka", "vankus"}, 0),
            new Word("Sky", new String[]{"medved", "bicykel", "obloha"}, 2),
            new Word("Certainly", new String[]{"urcite", "samozrejme", "necakane"}, 0),
            new Word("Enjoy your meal", new String[]{"ako vam chuti jedlo?", "dobru chut", "co si jedol?"}, 1),
            new Word("Deliberately", new String[]{"Schvalne, zamerne", "ddejako", "dokladne"}, 0),
            new Word("Obsessed", new String[]{"posadnuty", "znechuteny", "nahnevany"}, 0),
            new Word("Succeed", new String[]{"uspieť, podariť sa ", "prideliť ", "vyčerpať"}, 0),
            new Word("Valid", new String[]{"platný", "prideliť ", "nový"}, 0),
    };

    public DictionaryQuiz() {
         setup();
    }

    private void setup() {
        JFrame frame = new JFrame("Quiz");
        frame.setSize(500, 400);

        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        frame.setResizable(false);

        frame.add(makePanelTop(), BorderLayout.NORTH);
        frame.add(makePanelCenter(), BorderLayout.CENTER);
        frame.add(makePanelBottom(), BorderLayout.SOUTH);

        frame.setVisible(true);
        setWords();
    }

    private JPanel makePanelTop() {
        JPanel panel = new JPanel();
        labelTitle = new JLabel();
        labelTitle.setFont(f);
        panel.add(labelTitle);
        return panel;
    }

    private JPanel makePanelCenter() {
        JPanel panel = new JPanel();
        panel.setOpaque(true);
        panel.setBackground(Color.pink);
        buttonGroup = new ButtonGroup();
        panel.setOpaque(true);
        panel.setLayout(new GridLayout(3, 1));
        for (int i = 0; i < jRadioButtons.length; i++) {
            jRadioButtons[i] = new JRadioButton("");
            buttonGroup.add(jRadioButtons[i]);
            jRadioButtons[i].setFont(f);
            panel.add(jRadioButtons[i]);
        }

        return panel;
    }

    private JPanel makePanelBottom() {
        JPanel panel = new JPanel();
        btnSubmit = new JButton("Skontroluj");
        btnSubmit.setFocusable(false);
        btnSubmit.addActionListener(this);
        panel.add(btnSubmit);
        return panel;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        int correctWordPosition = words[position].correctWordPosition;
        String correctWord = words[position].option[correctWordPosition];

        if (canGoToNextQuestion) {
            btnSubmit.setText("Skontroluj");
            setWords();
            buttonGroup.clearSelection();
            canGoToNextQuestion = false;
            return;
        }
        int i;
        for (i = 0; i < jRadioButtons.length; i++) {
            if (jRadioButtons[i].isSelected()) {
                break;
            }
        }
        if (i == jRadioButtons.length) {
            JOptionPane.showMessageDialog(null, "Niečo zaškrtni");
            return;
        }
        if (!jRadioButtons[i].getText().equals(correctWord)) {
            jRadioButtons[i].setBackground(Color.red);
        }
        jRadioButtons[correctWordPosition].setBackground(Color.green);
        canGoToNextQuestion = true;
        btnSubmit.setText("Ďalšia otázka");
    }

    private void setWords() {
        int firstRandomPos = r.nextInt(words.length);
        int secondRandomPos = r.nextInt(words.length);
        int thirdPosition = r.nextInt(words.length);
        int randPositionInArray = r.nextInt(2);
        position = r.nextInt(words.length);
        System.out.println(position);
        int correctPosition = words[position].correctWordPosition;

        String correctWord = words[position].option[words[position].correctWordPosition];
        labelTitle.setText("Prelož slovo : " + words[position].wordInTitle);

        String firstRandomWord = String.valueOf(words[firstRandomPos].option[randPositionInArray]);
        String secondRandomWord = String.valueOf(words[secondRandomPos].option[randPositionInArray]);
        String thirdRandomWord = String.valueOf(words[thirdPosition].option[randPositionInArray]);


        for (int i = 0; i < jRadioButtons.length; i++) {
            jRadioButtons[i].setBackground(defaultBackground);
        }
        jRadioButtons[0].setText(firstRandomWord);
        jRadioButtons[1].setText(secondRandomWord);
        jRadioButtons[2].setText(thirdRandomWord);
        jRadioButtons[correctPosition].setText(correctWord);
    }
}
